@extends('layout.master')

@section('page-title')
Welcome
@endsection

@section('card-title')
Selamat Datang
@endsection

@section('content') 
<h1 style="text-transform: uppercase">Selamat Datang! {{$fname}} {{$lname}}</h1>
<p><b>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</b></p>
@endsection