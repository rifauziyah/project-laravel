@extends('layout.master')

@section('page-title')
Register
@endsection

@section('card-title')
Sign Up Form
@endsection


@section('content') 
<form action="/welcome" method="POST">
  @csrf
  <!-- First Name --> 
  <label for="fname">First name:</label><br><br>
  <input type="text" name="fname" id="fname">
  <br><br>
  
  <!-- Last Name -->
  <label for="lname">Last name:</label><br><br>
  <input type="text" name="lname" id="lname">
  <br><br>

  <!-- Gender -->
  <label for="gender">Gender:</label><br><br>
  <input type="radio" name="gender" id="male" value="Male">
    <label for="male">Male</label><br>
  <input type="radio" name="gender" id="female" value="Female">
    <label for="female">Female</label><br>
  <input type="radio" name="gender" id="other" value="Other">
    <label for="other">Other</label>
  <br><br>

  <!-- Nationality -->
  <label for="nationality">Nationality:</label><br><br>
  <select name="nationality" id="nationality">
      <option value="Indonesia">Indonesia</option>
      <option value="Amerika">Amerika</option>
      <option value="Inggris">Inggris</option>
    </select>
  <br><br>

  <!-- Language Spoken -->
  <label for="languange">Language Spoken:</label><br><br>
  <input type="checkbox" name="languange" id="ID" value="Bahasa Indonesia">
    <label for="ID">Bahasa Indonesia</label><br>
  <input type="checkbox" name="languange" id="EN" value="English">
    <label for="EN">English</label><br>
  <input type="checkbox" name="languange" id="Other" value="Other">
    <label for="Other">Other</label>
  <br><br>

  <!-- Bio -->
  <label for="bio">Bio:</label><br><br>
  <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
  <br>
  
  <button type="submit" value="sign_up">Sign Up</button>
    
</form>
@endsection