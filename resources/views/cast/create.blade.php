@extends('layout.master')

@section('page-title')
Tambah Cast Baru
@endsection

@section('card-title')
Form Tambah Cast Baru
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group col-md-6">
            <label for="umur">Umur</label>
            <input type="number" name="umur" id="umur" min="1" class="form-control">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" class="form-control" id="bio" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection