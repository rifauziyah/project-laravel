@extends('layout.master')

@section('page-title')
Edit Detail {{$cast->nama}}
@endsection

@section('card-title')
Edit Detail Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control" value="{{$cast->nama}}">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group col-md-6">
            <label for="umur">Umur</label>
            <input type="number" name="umur" id="umur" min="1" class="form-control" value="{{$cast->umur}}">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" class="form-control" id="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection