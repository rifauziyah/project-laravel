@extends('layout.master')

@section('page-title')
Detail {{$cast->nama}}
@endsection

@section('card-title')
Detail Cast
@endsection

@section('content')
<h3>{{$cast->nama}}, {{$cast->umur}} tahun</h3>
<p>{{$cast->bio}}</p>
@endsection