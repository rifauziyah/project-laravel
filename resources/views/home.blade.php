@extends('layout.master')

@section('page-title')
Dashboard
@endsection

@section('card-title')
Media Online
@endsection


@section('content')    
<h2>Sosial Media Developer</h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit Join di Media Online</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer</li>
</ul>

<h3>Cara Bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection
